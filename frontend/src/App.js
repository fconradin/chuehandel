import React, {Component} from 'react';
import {Provider} from 'react-redux';
import { Page } from 'react-onsenui';
// import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Home from './routes/Home'
import store from './store'

import './App.css';
import {storeTokensInState} from "./store/actions/activeComponent";

store.dispatch(storeTokensInState());

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Home/>
            </Provider>
        )
    }
}

export default App;