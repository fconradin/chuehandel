import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css';
import {Card, Button} from "react-onsenui";

class StartGameButton extends Component {

    handleStartGame = () => {
        this.props.dispatch({type: '_S_START_GAME'})
    };

    renderConditions = () => {
        // console.log('rendering start', this.props);
        return (
            !this.props.gameRunning
        )
    };

    renderButton = () => {
        if (this.props.showStartButton) {
            return (
                <Button
                    onClick={this.handleStartGame}
                    modifier='large'
                >
                    Start Game
                </Button>
            )
        }
    };

    render() {
        if (this.renderConditions()) {
            return (
                <Card>
                    <p>Share this table number with your friends: {this.props.gameTableName}</p>
                    {this.renderButton()}
                </Card>
            )
        } else {
            return (
                <div></div>
            )
        }
    }
}

const mapStateToProps = (state) => ({
    // showStartButton: true,
    gameTableName: state.game.game_table_name,
    players: state.players,
    showStartButton: state.game.players.length >= 3,
    gameRunning: state.game.game_running,
});

export default connect(mapStateToProps)(StartGameButton)
