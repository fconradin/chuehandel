import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css'
import {List, ListItem, Checkbox, Button, Modal, Toast, ProgressCircular} from "react-onsenui";
import WaitingToast from "../WaitingToast";

class PaymentCardSelector extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedCardsId: {},
            error: ''
        };
    };

    handleSelection = (e) => {
        console.log(e.currentTarget.id);
        let obj = {...this.state.selectedCardsId};
        obj[e.currentTarget.id] = !obj[e.currentTarget.id];
        this.setState({
            selectedCardsId: obj,
        })
    };

    handleSubmit = e => {
        e.preventDefault();
        let selectedAmount = this.props.moneyStack.reduce((acc, cur) => {
            if (this.state.selectedCardsId[cur[0]]) {
                acc += cur[1]
            }
            return acc
        }, 0);

        if (selectedAmount < this.props.winningBid) {
            this.setState({error: `Your offer was ${this.props.winningBid}. ${selectedAmount} is less.`})
        } else {
            this.props.dispatch({
                type: '_S_COMPLETE_AUCTION',
                payload: {cards: this.state.selectedCardsId, current_animal: this.props.current_animal}
            });
            this.setState({selectedCardsId: {}})
        }
    };

    renderConditions = () => (
        this.props.auction.ended && this.props.private_player_name === this.props.winner
    );

    sedondaryRenderConditions = () => (
        this.props.auction.ended && this.props.private_player_name !== this.props.winner
    )

    hideModal = () => {
        this.setState({error: ''})
    };

    showModal = () => {
        console.log('error should be shown')
        return this.state.error !== ''
    };

    renderModal = () => {
        return (
            <Modal
                isOpen={this.showModal()}
            >
                <section style={{margin: '16px'}}>
                    <p style={{opacity: 0.6}}>
                        {this.state.error}
                    </p>
                    <p>
                        <Button onClick={this.hideModal}>
                            Close
                        </Button>
                    </p>
                </section>
            </Modal>)
    };

    renderCheckboxRow = (c) => (
        <ListItem
            key={c[0]}
            tappable
            id={c[0]}
            onClick={this.handleSelection}
        >
            <label className='left'>
                <Checkbox
                    onClick={this.handleSelection}
                    checked={this.state.selectedCardsId[c[0]] || false}
                />
            </label>
            <label htmlFor={c[0]} className='center'>
                {c[1]}
            </label>
        </ListItem>
    );


    render() {
        if (this.renderConditions()) {
            return (
                <div>
                    {this.renderModal()}
                    <List
                        dataSource={this.props.moneyStack}
                        renderRow={this.renderCheckboxRow}
                        renderHeader={() => <p>Select the cards to pay</p>}
                    />
                    <div className='payment_card_selector_pay_button_container'>
                        <Button onClick={this.handleSubmit} modifier='large'>Pay</Button>
                    </div>
                </div>
            )
        } else if (this.sedondaryRenderConditions()) {
            return (
                <WaitingToast
                    text={`${this.props.winner} is paying ...`}
                    isOpen={this.sedondaryRenderConditions()}
                />);
        } else {
            return null
        }
    }
}

const mapStateToProps = (state) => ({
    moneyStack: state.private_player.money_stack,
    winner: state.game.auction.winner,
    winningBid: state.game.bidders[state.game.auction.winner],
    current_animal: state.game.auction.current_animal,
    private_player_name: state.private_player.player_name,
    auction: state.game.auction
});

export default connect(mapStateToProps)(PaymentCardSelector)