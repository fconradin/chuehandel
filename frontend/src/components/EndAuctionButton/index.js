import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css'
import {Button} from "react-onsenui";

class EndAuction extends Component {
    renderConditions = () => (
        !this.props.auction.winner &&
        this.props.player_name === this.props.auction.auctioneer &&
        this.props.action === 'Auction' &&
        Object.keys(this.props.bidders).length > 0
    );


    handleEndAuction = () => {
        this.props.dispatch({type: '_S_END_AUCTION'})
    };

    render() {
        if (this.renderConditions()) {
            return (
                <div className='end_auction_button_container'>
                    <Button
                        onClick={this.handleEndAuction}
                        value='End'
                        modifier='large'
                    >
                        End auction
                    </Button>
                </div>
            )
        } else {
            return (<div></div>)
        }
    }
}

const mapStateToProps = (state) => ({
    player_name: state.private_player.player_name,
    auction: state.game.auction,
    bidders: state.game.bidders,
    action: state.game.action,
});

export default connect(mapStateToProps)(EndAuction)