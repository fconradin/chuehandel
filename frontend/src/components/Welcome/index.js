import React, {Component} from 'react';
import './index.css'
import {connect} from 'react-redux';
import {Page, Toolbar, Button, Input} from "react-onsenui";
import ErrorModal from "../ErrorModal"
import {BUTTON_STYLE} from "../../constants";
import {hostOrJoinGame} from "../../store/apiCalls";
import GameTable from "../GameTable";

class Welcome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            player_name: "",
            host: undefined,
            game_table_name: "",
        };
    }

    handleClick = (e) => {
        if (e.currentTarget.id === 'host') {
            this.setState({host: true})
        } else {
            this.setState({host: false})
        }
    };

    handleChange = e => {
        let obj = {};
        obj[e.currentTarget.id] = e.currentTarget.value;
        this.setState(obj);
    };

    handleSubmit = () => {
        let obj = {...this.state};
        if (obj.game_table_name === "") {
            delete obj['game_table_name']
        }
        this.props
            .dispatch(hostOrJoinGame(obj))
            .then(response => {
                console.log('handle submit', response);
                if (response) {
                    this.props.mainNavigator.pushPage({component: GameTable})
                }
            })
    };

    renderToolbar = () => (
        <Toolbar>
            <div className='center'>Chuehandel</div>
        </Toolbar>
    );

    render() {
        return (
            <Page renderToolbar={this.renderToolbar}>
                <ErrorModal/>
                <div className='welcome_container'>
                    <img className='welcome_image' src='cow.svg' alt="Cow picture"/>
                    <div className={
                        `welcome_host_game_container
                        ${(typeof this.state.host === 'undefined') ? 'welcome_hidden' : undefined}`}
                    >
                        <Input type="text"
                               placeholder="Your name"
                               value={this.state.player_name}
                               style={{textAlign: 'center'}}
                               onChange={this.handleChange}
                               id="player_name"
                        />
                        <Input type="text"
                               placeholder="Table name"
                               value={this.state.game_table_name}
                               onChange={this.handleChange}
                               style={this.state.host ? {'display': 'none'} : {}}
                               id="game_table_name"
                        />
                        <Button
                            onClick={this.handleSubmit}
                            modifier='large'
                        >
                            Let's go
                        </Button>
                    </div>
                    <div className='welcome_host_or_join_button_container'>
                        <Button
                            style={BUTTON_STYLE}
                            onClick={this.handleClick}
                            id='host'
                            disabled={!this.state.host ? undefined : "true"}
                        >
                            Host
                        </Button>
                        <Button
                            style={BUTTON_STYLE}
                            onClick={this.handleClick}
                            id='join'
                            disabled={(typeof this.state.host === "undefined") || this.state.host ? undefined : "true"}
                        >
                            Join
                        </Button>
                    </div>
                </div>
            </Page>
        )
    }
}


export default connect()(Welcome);