import React, {Component} from "react"
import {connect} from 'react-redux';
import {Page, Toolbar, BackButton, List, ListHeader, ListItem, ListTitle} from "react-onsenui";


class PublicPlayerOverview extends Component {
    renderToolbar = () => (
        <Toolbar>
            <div className='left'><BackButton/></div>
            <div className='center'>{this.props.player.player_name} </div>
        </Toolbar>
    );

    renderRow = (row, index) => (
        <ListItem key={index}>{row[0]}, Count: {row[1]}</ListItem>
    );


    render() {
        console.log(this.props);
        return (
            <Page renderToolbar={this.renderToolbar}>
                <List
                    dataSource={Object.entries(this.props.player.formated_animal_stack)}
                    renderRow={this.renderRow}
                    renderHeader={() => <ListTitle>Animals</ListTitle>}
                    renderFooter={() => <ListTitle>Money stack: {this.props.player.money_stack_size}</ListTitle>}
                />
            </Page>
        )
    }
}

const mapStateToProps = (state) => {
    console.log(Object.entries(state.private_player.formated_money_stack));
    return ({
        // game_table_name: state.game.game_table_name,
        // player_name: state.private_player.player_name,
        // formated_money_stack: Object.entries(state.private_player.formated_money_stack),
        // formated_animal_stack: Object.entries(state.private_player.formated_animal_stack)
    })
};

export default connect(mapStateToProps)(PublicPlayerOverview);