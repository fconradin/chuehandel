import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css'
import { Button } from "react-onsenui";
import WaitingToast from "../WaitingToast";

class StartAuctionButtons extends Component {
    handleAuctionSubmit = (e) => {
        console.log('click');
        console.log(e.currentTarget.id);
        this.props.dispatch({type: '_S_SET_ACTION', payload: e.currentTarget.id})
    };

    renderAuctionButton = () => {
        if (this.props.animal_cards_left > 0) {
            return (
                <Button
                    style={{margin: '6px', width:100}}
                    onClick={this.handleAuctionSubmit}
                    id='Auction'
                >
                    Auction
                </Button>
            )
        }
    };

    renderBluffButton = () => {
        console.log('process variables', process.env);
        if (this.props.auction.bluff_options.length > 0) {
            return (
                <Button
                    style={{margin: '6px', width:100}}
                    onClick={this.handleAuctionSubmit}
                    id='Bluff'
                >
                    Bluff
                </Button>
            )
        }
    };


    renderConditions = () => (
        this.props.player_name === this.props.auction.auctioneer && this.props.action === null
    );

    secondaryRenderConditions = () => (
        this.props.action === null
    )


    render() {
        if (this.renderConditions()) {
            return (
                <div>
                <div>Choose an action:</div>
                <div className='start_auctions_buttons'>
                    {this.renderAuctionButton()}
                    {this.renderBluffButton()}
                </div>
                    </div>
            )
        } else if (this.secondaryRenderConditions()) {
            return (
                <WaitingToast
                    text={`${this.props.auction.auctioneer} is thinking hard...`}
                    isOpen={this.secondaryRenderConditions()}
                />
            )
        } else {
            return null
        }
    }
}

const mapStateToProps = (state) => ({
    player_name: state.private_player.player_name,
    auction: state.game.auction,
    animal_cards_left: state.game.animal_cards_left,
    action: state.game.action
});

export default connect(mapStateToProps)(StartAuctionButtons)