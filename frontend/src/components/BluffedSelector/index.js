import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css'
import {ListItem, Radio, List, Button} from "react-onsenui";
import WaitingToast from "../WaitingToast";

class BluffedSelector extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    };

    renderConditions = () => (
        this.props.action === 'Bluff' &&
        this.props.auction.auctioneer === this.props.private_player.player_name &&
        !this.props.auction.bluffed
    );

    secondaryRenderConditions = () => (
        this.props.action === 'Bluff' &&
        this.props.auction.auctioneer !== this.props.private_player.player_name &&
        !this.props.auction.bluffed
    )

    // clickability = () => {
    // console.log('General Clickability');
    // console.log(this.props.action === 'Bluff');
    // console.log(this.props.auction.bluff_options.map(bluff_option => Object.keys(bluff_option)[0])
    //     .indexOf(this.props.player.player_name) > -1);
    // console.log(this.props.auction.auctioneer === this.props.private_player.player_name);
    // console.log(!(this.props.auction.auctioneer in this.props.bidders));
    // return (
    //     this.props.action === 'Bluff' &&
    // Creates a list of the player names in the bluff options
    // this.props.auction.bluff_options.map(bluff_option => Object.keys(bluff_option)[0])
    // // checks if the player is in the list--> player can be selected to bluff
    //     .indexOf(this.props.player.player_name) > -1 &&
    // this.props.auction.auctioneer === this.props.private_player.player_name
    // auctioneer hasn't placed a bet yet
    // !(this.props.auction.auctioneer in this.props.bidders)
    //     )
    // };

    handleSelection = (e) => {
        console.log(e.currentTarget.id);
        this.setState({
            selectedPlayer: e.currentTarget.id,
        })
    };

    renderCheckboxRow = (player) => (
        <ListItem
            key={player}
            tappable
            id={player}
            onClick={this.handleSelection}
        >
            <label className='left'>
                <Radio
                    onClick={this.handleSelection}
                    checked={this.state.selectedPlayer === player}
                />
            </label>
            <label htmlFor={player} className='center'>
                {player}
            </label>
        </ListItem>
    );

    renderButton = () => {
        if (this.state.selectedPlayer)
            return (
                <div className='bluffed_selector_button_container'>
                    <Button modifier='large' onClick={this.selectBluffed}>Do it</Button>
                </div>
            )
    };

    selectBluffed = (e) => {
        e.preventDefault();
        this.props.dispatch({type: '_S_SELECT_BLUFFED', payload: this.state.selectedPlayer})
    };


    render() {
        if (this.renderConditions()) {
            return (
                <div>
                    <List
                        dataSource={this.props.bluffOptions}
                        renderRow={this.renderCheckboxRow}
                        renderHeader={() => <p>Who would you like to bluff?</p>}
                    />
                    {this.renderButton()}
                </div>
            )
        } else if (this.secondaryRenderConditions()) {
            return (
                <WaitingToast
                    text={`${this.props.auction.auctioneer} is thinking hard...`}
                    isOpen={this.secondaryRenderConditions()}
                />
            )
        } else {
            return null
        }
    }
}

const mapStateToProps = (state) => ({
    // bidders: state.game.bidders,
    auction: state.game.auction,
    private_player: state.private_player,
    action: state.game.action,
    bluffOptions: [...new Set(state.game.auction.bluff_options.map(e => Object.keys(e)[0]))]
});

export default connect(mapStateToProps)(BluffedSelector)