import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css'

import StartAuctionButtons from '../StartAuctionButtons'
import PaymentCardSelector from '../PaymentCardSelector';
import BidderComponent from '../BidderComponent';
import EndAuctionButton from '../EndAuctionButton';
import BluffCardSelector from '../BluffCardSelector'
import BluffedSelector from '../BluffedSelector'
import AnimalSelector from '../AnimalSelector'
import PrivatePlayerBid from '../PrivatePlayerBid'
import Animals from '../Animals';
import {Card} from "react-onsenui";

class PrivatePlayer extends Component {
    renderCurrentAnimals = () => {
        return (
            <div>
                <div className='private_player_text'>Current animals:</div>
                <div className='private_player_text'>
                    <Animals/>
                    {/*{this.props.currentAnimal.map(i => `${Object.values(i)[0] || ''} `)}*/}
                </div>
            </div>)
    };

    renderConditions = () => {
        return this.props.gameRunning
    };

    render() {
        if (this.renderConditions()) {
            return (
                <Card>
                    <div className="private_player_container">
                        <div className='private_player_top_container'>
                            {this.renderCurrentAnimals()}
                            <PrivatePlayerBid/>
                        </div>
                        <hr/>
                        <StartAuctionButtons/>
                        {/*if private player auctioneer, action auction, and at least one bid => render end auction */}
                        <EndAuctionButton/>
                        {/*if private player not auctioneer and action is auction render BidderComponent*/}
                        <BidderComponent/>
                        {/*if private player wins acution, select cards to pay*/}
                        <PaymentCardSelector/>
                        {/*If auctioneer chooses to bluff, his bluff options will be presented*/}
                        <BluffedSelector/>
                        {/*if bluffed player has been selected, select the animal*/}
                        <AnimalSelector/>
                        {/*if private player bluffed or bluffing, select cards to bid*/}
                        <BluffCardSelector/>
                    </div>
                </Card>
            )
        } else {
            return (
                <div></div>
            )
        }
    }
}

const mapStateToProps = (state) => ({
    currentAnimal: state.game.auction.current_animal,
    gameRunning: state.game.game_running,
});

export default connect(mapStateToProps)(PrivatePlayer)