import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css'
import {Button, Modal} from 'react-onsenui';

class BidderComponent extends Component {

    prepareBid = (e) => {
        this.props.dispatch({type: 'BIDDING_STARTED'});
        const totalMoney = this.props.moneyStack.reduce((acc, e) => (acc + e[1]), 0);
        const current_bid = Math.max(this.props.unsubmittedBid, this.props.submittedBid);
        const amount = current_bid + Number(e.currentTarget.innerHTML);
        const payload = Math.min(amount, totalMoney);
        this.props.dispatch({type: 'STORE_UNSUBMITTED_BID', payload});
    };

    submitBid = () => {
        const array_bids = Object.values(this.props.bidders);
        const current_max = Math.max(...array_bids);
        const amount = Math.max(this.props.unsubmittedBid, this.props.submittedBid);
        if (amount <= current_max) {
            this.props.dispatch({type: '_R_BIDDING_ERROR'})
        } else {
            const player_name = this.props.player_name;
            this.props.dispatch({type: '_R_CLEAR_ERRORS'});
            this.props.dispatch({type: '_S_SUBMIT_BID', payload: {amount, player_name}});
            this.props.dispatch({type: 'STORE_UNSUBMITTED_BID', payload:0})
        }
    };

    renderConditions = () => (
        !this.props.auction.winner &&
        this.props.action === 'Auction' &&
        this.props.game_running
    );

    buttonDisabled = () => {
      return this.props.player_name === this.props.auction.auctioneer ? true : false
    };

    hideModal = () => {
        this.props.dispatch({type: '_R_CLEAR_ERRORS'})
    };

    showModal = () => {
        return this.props.biddingError !== '';
    };

    renderModal = () => {
        return (
            <Modal
                isOpen={this.showModal()}
            >
                <section style={{margin: '16px'}}>
                    <p style={{opacity: 0.6}}>
                        {this.props.biddingError}
                    </p>
                    <p>
                        <Button onClick={this.hideModal}>
                            Close
                        </Button>
                    </p>
                </section>
            </Modal>)
    };

    render() {
        if (this.renderConditions()) {
            return (
                <div>
                    {this.renderModal()}
                    <div className="button-bar bidder_component_bid_container">
                        <div className="button-bar__item">
                            <button className="button-bar__button bidder_component_button"
                                    onClick={this.prepareBid} disabled={this.buttonDisabled()}>-10
                            </button>
                        </div>
                        <div className="button-bar__item">
                            <button className="button-bar__button bidder_component_button"
                                    onClick={this.prepareBid} disabled={this.buttonDisabled()}>+10
                            </button>
                        </div>
                        <div className="button-bar__item">
                            <button className="button-bar__button bidder_component_button"
                                    onClick={this.prepareBid} disabled={this.buttonDisabled()}>+50
                            </button>
                        </div>
                    </div>
                    <div>
                        <button
                            className="button--large--cta"
                            // style="width: 95%; margin: 0 auto;"
                            onClick={this.submitBid}
                            disabled={this.buttonDisabled()}
                        >
                            Place bid
                        </button>
                    </div>
                </div>
            )
        } else {
            return (<div></div>)
        }
    }
}

const mapStateToProps = (state) => ({
    game_running: state.game.game_running,
    moneyStack: state.private_player.money_stack,
    auction: state.game.auction,
    action: state.game.action,
    bidders: state.game.bidders,
    player_name: state.private_player.player_name,
    biddingError: state.errors.bidding,
    biddingStarted: state.game.biddingStarted || state.private_player.player_name in state.game.bidders,
    submittedBid: state.private_player.player_name in state.game.bidders
        ? state.game.bidders[state.private_player.player_name]
        : 0,
    unsubmittedBid: state.game.bid
});

export default connect(mapStateToProps)(BidderComponent)