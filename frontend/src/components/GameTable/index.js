import React, {Component} from "react"
import {connect} from 'react-redux';
import {createSocket} from "../../store/websocket";
import StartGameButton from '../StartGameButton';
import PrivatePlayer from '../PrivatePlayer';
import Welcome from '../Welcome';
import EndGame from '../EndGame';
import PrivatePlayerOverview from '../PrivatePlayerOverview';
import './index.css';
import {Page, Toolbar, ToolbarButton, Icon} from "react-onsenui";
import PublicPlayerItem from "../PublicPlayerItem";
import {BASE_WS_URL} from "../../constants";

class GameTable extends Component {
    componentDidMount() {
        // console.log("GameTableComponent creating websocket connection once component mounted")
        const adr = `${BASE_WS_URL}/ws/table/${this.props.game_table_name}/?${this.props.token}`;
        // console.log('logging adr', adr);
        createSocket(adr, this.props.mainNavigator)
    };

    renderPlayers = () => {
        // console.log('rendering players', this.props);
        if (this.props.players.length > 1) {
            return (this.props.players.map((p, i) => {
                // console.log('renderPlayer Game table', p);
                if (p.player_name !== this.props.private_player.player_name) {
                    return <PublicPlayerItem key={i} player={p} mainNavigator={this.props.mainNavigator}/>
                }
            }))
        }
    };

    handleClick = (e) => {
        // console.log('Icon clicked');
        // console.log(this.props);
        this.props.mainNavigator.pushPage({
            component: PrivatePlayerOverview,
        });
    };

    renderToolbar = () => (
        <Toolbar>
            <div className='center'>{this.props.private_player.player_name}</div>
            <div className='right'>
                <ToolbarButton>
                    <Icon onClick={this.handleClick} icon='ion-navicon, material:md-menu'/>
                </ToolbarButton>
            </div>
        </Toolbar>
    );

    render() {
        if (this.props.gameEnded) {
            localStorage.clear();
            this.props.mainNavigator.pushPage({component: EndGame});
            return null;
        } else
            return (
                <Page renderToolbar={this.renderToolbar}>
                    <StartGameButton/>
                    <PrivatePlayer/>
                    <div>
                        {this.renderPlayers()}
                    </div>
                </Page>
            )
    }
}

const mapStateToProps = (state) => ({
    token: state.user.token,
    game_table_name: state.user.game_table_name,
    players: state.game.players,
    private_player: state.private_player,
    gameEnded: state.game.gameEnded,
});

export default connect(mapStateToProps)(GameTable);
