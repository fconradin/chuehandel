import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css'

class PrivatePlayerBid extends Component {
    renderConditions = () => (
        this.props.biddingStarted
    )


    render() {
        console.log('private player bid', this.props.unsubmittedBid, this.props.submittedBid);
        if (this.renderConditions()) {
            return (
                <div>
                    <div className='private_player_bid_text'>
                        Your bid:
                    </div>
                    <div className='private_player_bid_bid_container'>
                        <div className='private_player_bid_text'>
                            {Math.max(this.props.unsubmittedBid, this.props.submittedBid)}
                        </div>
                    </div>
                </div>
            )
        } else {
            return (<div></div>)
        }
    }
}

const mapStateToProps = (state) => ({
    unsubmittedBid: state.game.bid,
    submittedBid: state.private_player.player_name in state.game.bidders
        ? state.game.bidders[state.private_player.player_name]
        : 0,
    biddingStarted: state.game.biddingStarted || state.private_player.player_name in state.game.bidders,
});

export default connect(mapStateToProps)(PrivatePlayerBid)