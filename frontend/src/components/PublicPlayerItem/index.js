import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css'
import {ListItem, Card} from "react-onsenui";
import PublicPlayerOverview from "../PublicPlayerOverview";
import '../../animals_font/flaticon.css'
import Animals from '../Animals';

class PublicPlayerItem extends Component {
    renderBid = () => {
        if (this.props.auction.auctioneer === this.props.player.player_name) {
            return <div>Auctioneer</div>
        } else if (this.props.action === 'Auction') {
            const bid = this.props.player.player_name in this.props.bidders
                ? this.props.bidders[this.props.player.player_name]
                : '';
            return <span style={{background: '#FB6542'}} className="notification right">{bid}</span>
        }
    };

    handleClick = (e) => {
        console.log('Icon clicked');
        console.log(this.props);
        this.props.mainNavigator.pushPage(
            {component: PublicPlayerOverview, props: {player: this.props.player}})
    };

    isAuctioneer = () => (
        this.props.auction.auctioneer === this.props.player.player_name
    );

    renderAction = () => {
        if (this.isAuctioneer()) {
            return <div>{this.props.action}</div>
        }
    };

    render() {
        const card_height = 40;
        const x_pad = 1;
        const y_pad = 6;
        const x_offset = 7;
        const y_offset = 3;
        const cards = [...Array(this.props.player.money_stack_size).keys()];
        const height = card_height + 2 * y_pad + (this.props.player.money_stack_size - 1) * y_offset;
        return (
            <Card
                onClick={this.handleClick}
                style={this.isAuctioneer() ? {background: '#FB6542', color: 'white'} : undefined}>
                <div className='public_player_card'>
                    <div>{this.props.player.player_name}</div>
                    {this.renderAction()}
                    {this.renderBid()}
                </div>
                <svg xmlns="http://www.w3.org/2000/svg" height={height}>
                    {cards.map((v, i) => (
                        <rect
                            key={i}
                            x={x_offset * i + x_pad}
                            y={y_offset * i + y_pad}
                            rx="2px"
                            ry="2px"
                            width="30px"
                            height={card_height}
                            fill="white"
                            stroke="black"
                        />
                    ))}
                </svg>
                <div>
                    {this.props.player.formated_animal_stack.map((c, i) =>
                        (
                            <Animals/>
                            // <span key={i} className='public_player_item_animal_container'>
                            //     <span>{c['total']}</span>
                            //     <i className={`${c['ac_icon']} icon`}/>
                            //     <span>{c['ac_points']}</span>
                            // </span>
                        )
                    )}

                </div>
            </Card>
        )
    }
}

const mapStateToProps = (state) => {
    return ({
        auction: state.game.auction,
        bidders: state.game.bidders,
        action: state.game.action,
    })
};

export default connect(mapStateToProps)(PublicPlayerItem)
