import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css'

const Animals = (props) => (
    Object.keys(props.animals).map((k, i) => (
        <span key={i} className='public_player_item_animal_container'>
            <span>{props.animals[k]['points']}</span>
            <i className={`${props.animals[k]['icon']} icon`}/>
        </span>
        )
    )
);
const mapStateToProps = (state) => ({
    animals: state.game.auction.current_animal
});

export default connect(mapStateToProps)(Animals)

