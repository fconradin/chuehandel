import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css'
import {List, Button, ListItem, Radio} from "react-onsenui";
import WaitingToast from "../WaitingToast";

class AnimalSelector extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    };

    renderConditions = () => {
        // console.log('Animal card selector render conditions');
        // console.log(this.props.auction.bluffed !== '');
        // console.log(this.props.auction.auctioneer === this.props.privatePlayer.player_name);
        // console.log(this.props.auction.current_animal.length === 0);

        return (
            this.props.auction.bluffed !== null &&
            this.props.auction.auctioneer === this.props.privatePlayer.player_name &&
            this.props.auction.current_animal.length === 0
        )
    };

    secondaryRenderConditions = () => (
        this.props.auction.bluffed !== null &&
        this.props.auction.auctioneer !== this.props.privatePlayer.player_name &&
        this.props.auction.current_animal.length === 0
    );

    selectAnimal = e => {
        e.preventDefault();
        this.props.dispatch({type: '_S_SELECT_ANIMAL', payload: this.state.selectedAnimal})
    };

    renderButton = () => {
        if (this.state.selectedAnimal)
            return (
                <div className='animal_selector_button_container'>
                    <Button modifier='large' onClick={this.selectAnimal}>Do it</Button>
                </div>
            )
    };

    renderCheckboxRow = (animal) => (
        <ListItem
            key={animal}
            tappable
            id={animal}
            onClick={this.handleSelection}
        >
            <label className='left'>
                <Radio
                    onClick={this.handleSelection}
                    checked={this.state.selectedAnimal === animal}
                />
            </label>
            <label htmlFor={animal} className='center'>
                {animal}
            </label>
        </ListItem>
    );

    handleSelection = (e) => {
        console.log(e.currentTarget.id);
        this.setState({
            selectedAnimal: e.currentTarget.id,
        })
    };


    render() {
        // console.log('the bluff options are', this.props.bluffOptions);
        if (this.renderConditions()) {
            return (
                <div>
                    <List
                        dataSource={this.props.bluffOptions}
                        renderRow={this.renderCheckboxRow}
                        renderHeader={() => <p>Select an animal!</p>}
                    />
                    {this.renderButton()}
                </div>
            )
        } else if (this.secondaryRenderConditions()) {
            return (
                <WaitingToast
                    text={`${this.props.auction.auctioneer} is thinking hard...`}
                    isOpen={this.secondaryRenderConditions()}
                />
            )
        } else {
            return null
        }
    }
}

const mapStateToProps = (state) => {
    const listOfBluffOptions = state.game.auction.bluff_options
        .filter(e => Object.keys(e)[0] === state.game.auction.bluffed);
    return ({
        auction: state.game.auction,
        privatePlayer: state.private_player,
        bluffOptions: [... new Set(listOfBluffOptions.map(e => Object.values(e)[0]))]
    })
};

export default connect(mapStateToProps)(AnimalSelector)