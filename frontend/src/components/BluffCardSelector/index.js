import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css'
import {Button, ListItem, Checkbox, List} from "react-onsenui";
import WaitingToast from "../WaitingToast";

class BluffCardSelector extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedCardsId: {},
        };
    };

    handleSelection = (e) => {
        let obj = {...this.state.selectedCardsId};
        obj[e.currentTarget.id] = !obj[e.currentTarget.id];
        this.setState({
            selectedCardsId: obj,
        })
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.dispatch({
            type: '_S_COMPLETE_BLUFF',
            payload: {cards: this.state.selectedCardsId, player_name: this.props.private_player_name}
        });
        this.setState({selectedCardsId: {}})
    };

    renderConditions = () => {
        // console.log('Bluff card selector conditions', this.props.action === 'Bluff');
        // console.log((this.props.private_player_name === this.props.auction.auctioneer
        //         || this.props.private_player_name === this.props.auction.bluffed));
        // console.log((this.props.auction.current_animal.length)>0);
        // console.log(!(this.props.private_player_name in this.props.bidders));
        return (
            this.props.action === 'Bluff'
            && (this.props.private_player_name === this.props.auction.auctioneer
                || this.props.private_player_name === this.props.auction.bluffed)
            && this.props.auction.current_animal.length > 0
            && !(this.props.private_player_name in this.props.bidders)
        )
    };

    secondaryRenderConditions = () => (
        this.props.action === 'Bluff'
        && (this.props.private_player_name !== this.props.auction.auctioneer
            || this.props.private_player_name !== this.props.auction.bluffed)
        && this.props.auction.current_animal.length > 0
        && !(this.props.private_player_name in this.props.bidders)
    );

    tertiaryRenderConditions = () => (
        this.props.action === 'Bluff'
        && (this.props.private_player_name === this.props.auction.auctioneer
            || this.props.private_player_name === this.props.auction.bluffed)
        && this.props.auction.current_animal.length > 0
        && (this.props.private_player_name in this.props.bidders)
    );

    renderCheckboxRow = (c) => (
        <ListItem
            key={c[0]}
            tappable
            id={c[0]}
            onClick={this.handleSelection}
        >
            <label className='left'>
                <Checkbox
                    onClick={this.handleSelection}
                    checked={this.state.selectedCardsId[c[0]] || false}
                />
            </label>
            <label htmlFor={c[0]} className='center'>
                {c[1]}
            </label>
        </ListItem>
    );

    render() {
        if (this.renderConditions()) {
            return (
                <div>
                    {/*{this.props.moneyStack.map(c => {*/}
                    {/*return (*/}
                    {/*<div key={c[0]}>*/}
                    {/*<input onChange={this.handleSelection}*/}
                    {/*type="checkbox"*/}
                    {/*id={c[0]}*/}
                    {/*value={c[1]}*/}
                    {/*checked={this.state.selectedCardsId[c[0]] || false}*/}
                    {/*/>*/}
                    {/*<label htmlFor={c[0]}>{c[1]}</label>*/}
                    {/*</div>*/}
                    {/*)*/}
                    {/*})}*/}
                    <List
                        dataSource={this.props.moneyStack}
                        renderRow={this.renderCheckboxRow}
                        renderHeader={() => <p>Select the cards to bluff</p>}
                    />
                    <br/>
                    <Button onClick={this.handleSubmit} modifier='large'>Bluff</Button>
                </div>
            )
        } else if (this.secondaryRenderConditions()) {
            return (
                <WaitingToast
                    text={`Other players are crackin' up a cold one...`}
                    isOpen={this.secondaryRenderConditions()}
                />
            )
        } else if (this.tertiaryRenderConditions()) {
            return (
                <WaitingToast
                    text={`It's not chess, but some players don't know that...`}
                    isOpen={this.tertiaryRenderConditions()}
                />
            )
        } else {
            return null
        }
    }
}

const mapStateToProps = (state) => ({
    moneyStack: state.private_player.money_stack,
    current_animal: state.game.auction.current_animal,
    private_player_name: state.private_player.player_name,
    auction: state.game.auction,
    bidders: state.game.bidders,
    action: state.game.action,
});

export default connect(mapStateToProps)(BluffCardSelector)