import React, {Component} from "react"
import {connect} from 'react-redux';
import {Page, Toolbar, BackButton, List, ListItem, ListTitle} from "react-onsenui";
import "./index.css"


class PrivatePlayerOverview extends Component {
    renderToolbar = () => (
        <Toolbar>
            <div className='left'><BackButton/></div>
            <div className='center'>{this.props.player_name} </div>
        </Toolbar>
    );

    render() {
        return (
            <Page renderToolbar={this.renderToolbar}>
                <ListTitle>Money</ListTitle>
                <div className="private_player_overview_money_container">
                    <svg xmlns="http://www.w3.org/2000/svg" height={this.props.svgHeight}>
                        {this.props.moneyStack.map(
                            (cards, j, arr) => {
                                let offset = arr.slice(0, j).reduce((acc, currV) => acc + currV.length * 7, 0) + j * 30;
                                return (cards.map(
                                    (card, i) => {
                                        return (
                                            <g key={`${j}_${i}`}>
                                                <rect
                                                    x={offset + 7 * i + 1}
                                                    y={10 * i + 6}
                                                    rx="2px"
                                                    ry="2px"
                                                    width="30px"
                                                    height="40px"
                                                    fill="white"
                                                    stroke="black"
                                                />
                                                <text x={offset + 7 * i + 5} y={10 * i + 20} fontFamily="Verdana"
                                                      fontSize="14"
                                                      fontWeight="bold"
                                                      fill="black">{card}
                                                </text>
                                            </g>
                                        )
                                    }
                                ))
                            }
                        )}
                    </svg>
                </div>
                <List
                    dataSource={this.props.formated_animal_stack}
                    renderRow={this.renderRow}
                    renderHeader={() => <ListTitle>Animals</ListTitle>}
                />
                <ListTitle>Cards left: {this.props.animal_cards_left}</ListTitle>
            </Page>
        )
    }
}

const mapStateToProps = (state) => {
    let moneyStack = [];
    state.private_player.money_stack.map(i => {
        let arr = moneyStack[moneyStack.length - 1] || [];
        if (i[1] === arr[0]) {
            moneyStack[moneyStack.length - 1].push(i[1])
        } else {
            moneyStack.push([i[1]])
        }
    });
    let maxAmountSameCard = moneyStack.reduce((acc, currV) => Math.max(acc, currV.length),0);
    let svgHeight = 40 + (maxAmountSameCard - 1)*10 + 12;
    return ({
        moneyStack,
        svgHeight,
        game_table_name: state.game.game_table_name,
        player_name: state.private_player.player_name,
        formated_animal_stack: Object.entries(state.private_player.formated_animal_stack),
        animal_cards_left: state.game.animal_cards_left
    })
};

export default connect(mapStateToProps)(PrivatePlayerOverview);