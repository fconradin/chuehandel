import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css'
import {Card, Button} from "react-onsenui";
import Welcome from '../Welcome';

class EndGame extends Component {
    restartGame = () => {
        this.props.mainNavigator.pushPage({component:Welcome})};

    render() {
        return (
            <div className='end_game_container'>
                <Card>
                    <div className='end_game_content_container'>
                        <div className='end_game_message'>Thanks for playing</div>
                        <div className='end_game_button_container'>
                            <Button onClick={this.restartGame} modifier='large'>Start Over</Button>
                        </div>
                    </div>
                </Card>

            </div>
        )
    }
}

// const mapStateToProps = (state) => ({
//     winner: "you win"
// });

export default connect()(EndGame)