import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css'
import {Modal, Button} from "react-onsenui";

class ErrorModal extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    };

    hideModal = () => {
        this.props.dispatch({type: '_R_CLEAR_ERRORS'})
    };

    showModal = () => {
        return this.props.errorMessage !== ''
    };

    render() {
        return (
            <Modal
                isOpen={this.showModal()}
            >
                <section style={{margin: '16px'}}>
                    <p style={{opacity: 0.6}}>
                        {this.props.errorMessage}
                    </p>
                    <p>
                        <Button onClick={this.hideModal}>
                            Close
                        </Button>
                    </p>
                </section>
            </Modal>)
    };
}

const mapStateToProps = (state) => ({
    errorMessage: state.errors.joiningOrHosting
});

export default connect(mapStateToProps)(ErrorModal)




