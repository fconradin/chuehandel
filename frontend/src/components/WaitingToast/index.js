import React, {Component} from 'react';
import './index.css';
import {Toast, ProgressCircular} from "react-onsenui";

const WaitingToast = (props) => (
    <Toast isOpen={props.isOpen}>
        <div className='waiting_toast_container'>
            <div>
                {props.text}
            </div>
            <div>
                <ProgressCircular indeterminate/>
            </div>
        </div>
    </Toast>);

export default WaitingToast;