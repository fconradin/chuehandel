import store from './index.js'
import './index.js'
import Welcome from "../components/Welcome";

let socket = null;

const handleMessage = e => {
    const parsedData = JSON.parse(e.data);
    console.log('websocket.js: receiving data from backend', parsedData);
    store.dispatch({...parsedData})
};

const createSocket = (adr, navigator) => {
    console.log("websocket.js create socket");
    const myInt = setInterval(() => {
        if (socket === null || socket.readyState > 1) {
            // console.log("websocket has not been created or is closed or closing")
            socket = new WebSocket(adr);
            socket.onmessage = handleMessage;
            socket.onclose = e => {
                if (e.code === 4030) {
                    // console.log("unauthorized")
                    localStorage.clear();
                    clearInterval(myInt);
                    navigator.pushPage({component:Welcome})
                }
            }
        } else if (socket.readyState === 1) {
            // console.log("attaching onclose method that handles loss of connection. If this is attached before connection is open, it will be called without waiting for an interval")
            socket.onclose = e => {
                if (e.code !== 1000) {
                    createSocket(adr)
                }
            };
            // store.dispatch(storeSocketToState(socket));
            clearInterval(myInt);
        } else {
            // console.log("ready state 0")
            // the connection is still opening
        }
    }, 1000)
};

export {socket, createSocket}

