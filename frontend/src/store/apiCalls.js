import { saveTokenAndGameTableName } from "./actions/activeComponent";
import {BASE_URL} from "../constants";

const handleErrors = response => {

    if (!response.ok) {
        throw response;
    }
    return response.json();
};

export const hostOrJoinGame = (data) => dispatch => {

    let myHeaders = new Headers({
        "Content-Type": "application/json"
    });

    let config = {
        method: "POST",
        headers: myHeaders,
        body: JSON.stringify(data)
    };

    return fetch(`${BASE_URL}/backend/api/game/`, config)
        .then(handleErrors)
        .then(data => {
            dispatch(saveTokenAndGameTableName(data.token, data.game_table_name));
        })
        .catch(response => {
            return response.json()
        })
        .then(data => {
            if (data)  {
                dispatch({type:'ERROR_JOINING_OR_HOSTING', payload: data.error[0]});
            } else {
                return true
            }
        })
};