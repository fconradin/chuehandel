import {combineReducers} from 'redux';
import {socket} from '../websocket'
import Welcome from "../../components/Welcome";

const send_info_to_backend = (obj) => {
    socket.send(JSON.stringify(obj));
};

let initialState = {
    action: null,
    animal_cards_left: null,
    bid:0,
    bidders: {},
    biddingStarted: false,
    auction: {
        auctioneer: null,
        bluffed: null,
        bluff_options: [{}],
        current_animal: [{}],
        ended: false,
        winner: null
    },
    game_running: false,
    game_table_name: null,
    players: [{player_name: null, formated_animal_stack: {}, money_stack_size: ''}]
};

const game = (state = initialState, action) => {
    switch (action.type) {
        case '_S_START_GAME':
            send_info_to_backend({...action});
            return {...state, game_running: true};
        case '_R_GAME_INFO':
            // console.log('in game reducer _R_GAME_INFO', action.payload);
            return {...action.payload, bid:state.bid, biddingStarted:state.biddingStarted, gameEnded:false};
        case '_S_SET_ACTION':
            send_info_to_backend({...action});
            return {...state, auction: {...state.auction, action: action.payload}};
        case 'BIDDING_STARTED':
            return {...state, biddingStarted: true};
        case 'STORE_UNSUBMITTED_BID':
            return {...state, bid: action.payload};
        case '_S_SUBMIT_BID':
            send_info_to_backend({...action});
            const bidders = {...state.bidders};
            bidders[action.payload.player_name] = action.payload.amount;
            return {...state, bidders};
        case '_S_END_AUCTION':
            send_info_to_backend({...action});
            const max_bid = Math.max(...Object.values(state.bidders));
            const winner = Object.entries(state.bidders).filter(i => i[1] === max_bid);
            const winner_name = winner[0][0];
            return {...state, biddingStarted: false, bid: 0, auction: {...state.auction, ended: true, winner: winner_name}};
        // case '_S_COMPLETE_AUCTION':
        //     // todo: update redux state to minimize wait for backend (info already sent to
        //     // backend in private_player
        //     return state;
        case '_S_SELECT_BLUFFED':
            send_info_to_backend({...action});
            return {...state, auction: {...state.auction, bluffed: action.payload}};
        case '_S_SELECT_ANIMAL':
            // todo: update redux state to minimize wait for backend (info already sent to
            // backend in private_player
            send_info_to_backend({...action});
            return state;
        case '_S_COMPLETE_BLUFF':
            send_info_to_backend({...action});
            return state;
        case '_R_END_GAME':
            return {...state, gameEnded: true};
        default:
            return state
    }
};

const selectedPlayer = (state = 'Dude', action) => {
    return state
};

// const unsubmittedInfo = (state = {bid: 0, biddingStarted: false}, action) => {
//     switch (action.type) {
//         case 'STORE_UNSUBMITTED_BID':
//             return {...state, bid: action.payload};
//         case 'BIDDING_STARTED':
//             return {...state, biddingStarted: true};
//         default:
//             return state
//     }
// };


const errors = (state = {joiningOrHosting: '', bidding: '', biddingStarted: false}, action) => {
    switch (action.type) {
        case 'ERROR_JOINING_OR_HOSTING':
            return {...state, joiningOrHosting: action.payload};
        case '_R_BIDDING_ERROR':
            return {...state, bidding: 'Your offer must be higher than the highest offer!'};
        case '_R_CLEAR_ERRORS':
            return {joiningOrHosting: '', bidding: '', biddingStarted: false};
        default:
            return state
    }
};

const user = (state = {}, action) => {
    // console.log("in user reducer")
    switch (action.type) {
        case "SAVE_TOKEN_AND_GAME_TABLE_NAME":
            localStorage.setItem('token', action.payload.token);
            localStorage.setItem('game_table_name', action.payload.game_table_name);
            return {token: action.payload.token, game_table_name: action.payload.game_table_name};
        case 'STORE_TOKENS_IN_STATE':
            // console.log("reducer store tokens in state")
            const token = localStorage.getItem('token');
            const game_table_name = localStorage.getItem('game_table_name');
            return {token, game_table_name};
        default:
            return state
    }
};

// const socket_connection = (state = null, action) => {
//     switch (action.type) {
//         case 'STORE_SOCKET_IN_STATE':
//             return action.payload.socket;
//         default:
//             return state
//     }
// };

const activeComponent = (state = Welcome, action) => {
    // console.log("in activeComponent reducer", action.type)
    switch (action.type) {
        case 'ACTIVATE_GAMETABLE':
            return "GameTable";
        case 'ACTIVATE_WELCOME':
            try {
                socket.close(1000)
            } catch (e) {
                console.log("reducers: cannot close socket connection gracefully");
                console.log("potentially player deleted in DB")
            }
            localStorage.clear();
            return Welcome;
        case 'ACTIVATE_HOST':
            return 'Host';
        case 'ACTIVATE_JOIN':
            return 'Join';
        case '_R_END_GAME':
            return 'EndGame';
        default:
            return state;
    }
};

const private_player = (state = {
    player_name: "",
    animal_stack: [],
    money_stack: [],
    formated_animal_stack: {},
    formated_money_stack: {}
}, action) => {
    // console.log("private player reducer", action.type, action.payload)
    switch (action.type) {
        case '_R_PRIVATE_PLAYER_INFO':
            return action.payload;
        case '_S_COMPLETE_AUCTION':
            send_info_to_backend({...action});
            const money_stack = state.money_stack.filter(c => !action.payload.cards[c[0]]);
            const animal_stack = [...state.animal_stack, ...action.payload.current_animal];
            let formated_animal_stack = {...state.formated_animal_stack};
            animal_stack.forEach(e => {
                if (formated_animal_stack[Object.values(e)[0]]) {
                    formated_animal_stack[Object.values(e)[0]] += 1
                } else {
                    formated_animal_stack[Object.values(e)[0]] = 1
                }
            });
            let formated_money_stack = {};
            money_stack.forEach(e => {
                if (formated_money_stack[e[1]]) {
                    formated_money_stack[e[1]] += 1
                } else {
                    formated_money_stack[e[1]] = 1
                }
            });
            return {...state, money_stack, animal_stack, formated_animal_stack, formated_money_stack};
        default:
            return state;
    }
};

const reducer = combineReducers({
    game,
    user,
    errors,
    selectedPlayer,
    private_player,
});

export {reducer}
