// export const activeComponent = (currentTargetId) => {
//     return {
//         type: 'ACTIVATE_COMPONENT',
//         payload: currentTargetId
//     };
// };

export const saveTokenAndGameTableName = (token, game_table_name) => {
    return {
        type: 'SAVE_TOKEN_AND_GAME_TABLE_NAME',
        payload: {token, game_table_name}
    }
};

export const storeTokensInState = () => {
    const token = localStorage.getItem("token");
    const game_table_name = localStorage.getItem("game_table_name");
    return {
        type: 'STORE_TOKENS_IN_STATE',
        payload: {token, game_table_name}
    }
};

// export const storeSocketToState = (socket) => {
//     return {
//         type: 'STORE_SOCKET_IN_STATE',
//         payload: {socket}
//     }
// };

export const updatePlayers = (player_list) => {
    console.log("action creator", player_list);
    return {
        type: 'UPDATE_PLAYER_LIST',
        payload: {player_list}
    }
};
