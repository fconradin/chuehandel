import React, {Component} from "react";

import "./index.css";
import GameTable from "../../components/GameTable";
import EndGame from "../../components/EndGame";
import PrivatePlayerOverview from "../../components/PrivatePlayerOverview";
import Welcome from "../../components/Welcome";
import {Navigator} from "react-onsenui";


class Home extends Component {
    pushPage(route, navigator, props = {}) {
        route.props = props;
        navigator.pushPage(route);
    }

    renderPage(route, navigator) {
        const props = route.props || {};
        props.mainNavigator = navigator;
        const componentOptions = {
            key: route.title,
            navigator,
            pushPage: this.pushPage
        };
        return React.createElement(route.component, props, componentOptions);
    }

    chosePage = () => {
        if (localStorage.getItem('game_table_name')) {
            return GameTable
        } else {
            return Welcome
        }

    };

    render() {
        return (
            <Navigator
                renderPage={this.renderPage}
                animation='slide'
                initialRoute={{
                    component: this.chosePage(),
                }}>
            </Navigator>)
    }
}

export default Home;