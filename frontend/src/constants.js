let BASE_URL;
let BASE_DOMAIN;
let BASE_WS_URL;
if (process.env.NODE_ENV === 'development') {
    BASE_DOMAIN = 'localhost:8090';
} else {
    BASE_DOMAIN = 'chuehandel.conradin.ch';
}
BASE_URL = `http://${BASE_DOMAIN}`;
BASE_WS_URL = `ws://${BASE_DOMAIN}`;

const BUTTON_STYLE = {margin: '6px', width:'100px', textAlign:'center'};

export
{
    BASE_URL,
    BASE_WS_URL,
    BUTTON_STYLE,
}
