from project.chuehandel.models.cards import MoneyCard, AnimalCard

from project.chuehandel.models.game import GameTable, Player, Auction, Bidder  # noqa

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'
    money_cards = {'Zero': {'count': 10, 'points': 0},
                   'Ten': {'count': 20, 'points': 10},
                   'Fifty': {'count': 10, 'points': 50},
                   'Hundred': {'count': 5, 'points': 100},
                   'Five-hundred': {'count': 5, 'points': 500}}

    animal_cards = {'Rooster': {'count': 4, 'points': 10, 'icon': 'flaticon-044-hen'},
                    'Goose': {'count': 4, 'points': 40, 'icon': 'flaticon-081-duck'},
                    'Cat': {'count': 4, 'points': 90, 'icon': 'flaticon-055-cat'},
                    'Monkey': {'count': 4, 'points': 160, 'icon': 'flaticon-013-monkey'},
                    'Sheep': {'count': 4, 'points': 250, 'icon': 'flaticon-032-sheep'},
                    'Goat': {'count': 4, 'points': 350, 'icon': 'flaticon-041-goat'},
                    'Donkey': {'count': 4, 'points': 500, 'icon': 'flaticon-037-donkey'},
                    'Pig': {'count': 4, 'points': 650, 'icon': 'flaticon-018-pig'},
                    'Cow': {'count': 4, 'points': 800, 'icon': 'flaticon-062-cow'},
                    'Horse': {'count': 4, 'points': 1000, 'icon': 'flaticon-059-horse'}}

    game_tables = [1000, 1001]

    players = [['Laurent', 1000, [51, 65, 70], 43], ['Colin', 1000, [49, 70, 72], 47],
               ['Robiel', 1000, [82, 69, 44], 56],
               ['Fred', 1001, [51, 65, 70], 43], ['Greg', 1001, [49, 70, 72], 47], ['Dude', 1001, [82, 69, 44], 56]]

    bidders = [[1, 'Laurent', [10, 20]], [1, 'Colin', [20, 30]], [1, 'Robiel', [30, 40]]]
    bidders2 = [[2, 'Fred', [10, 20]], [2, 'Greg', [20, 30]], [2, 'Dude', [30, 40]]]

    def generate_money(self):
        MoneyCard.objects.all().delete()
        for (key, value) in self.money_cards.items():
            for i in range(value['count']):
                card = MoneyCard(mc_name=key, mc_points=value['points'])
                card.save()

    def generate_animals(self):
        AnimalCard.objects.all().delete()
        for (key, value) in self.animal_cards.items():
            for i in range(value['count']):
                card = AnimalCard(ac_name=key, ac_points=value['points'], ac_icon=value['icon'])
                card.save()

    def generate_game_tables(self):
        for i in self.game_tables:
            g = GameTable.objects.create(game_table_name=i)
            g.set_default_animals()
            g.save()

    def generate_players(self):
        for i in self.players:
            p = Player.objects.create(player_name=i[0])
            p.game_table = GameTable.objects.get(game_table_name=i[1])
            a = AnimalCard.objects.filter(id__in=i[2])
            p.animal_stack.add(*a)
            p.set_default_money()
            p.save()

    def generate_auctions(self):
        for i in self.players:
            a = Auction.objects.create(auctioneer=Player.objects.get(player_name=i[0]),
                                       game_table=GameTable.objects.get(game_table_name=i[1]), action='auction')
            a.current_animal.add(AnimalCard.objects.get(id=i[3]))
            a.save()

    def generate_bidders(self):
        for y in [self.bidders, self.bidders2]:
            for i in y:
                for j in i[2]:
                    a = Auction.objects.all()
                    player = Player.objects.get(player_name=i[1])
                    Bidder.objects.create(auction=a[i[0]], player=player, amount=j)

    def game_setup(self):
        self.generate_game_tables()
        self.generate_players()
        self.generate_auctions()
        self.generate_bidders()

    # def see_if_auction_possible(self):
    #     auction = Auction.objects.get(id=211)
    #     auctioneer_animals = auction.auctioneer.animal_stack.values('ac_name').all()
    #     all_other_animals = auction.game_table.players.exclude(player_name=auction \
    # .auctioneer.player_name).values('animal_stack')
    #     id_all_owned_animals = [a['animal_stack'] for a in all_other_animals]
    #     names_all_owned_animals = AnimalCard.objects.filter(id__in=id_all_owned_animals).values('ac_name')
    #     auction_possible = auctioneer_animals.filter(ac_name__in=names_all_owned_animals).count()>0

    def get_bluff_options(self):
        bluff_options = []
        auction = Auction.objects.get(id=211)
        auctioneer_animals = [a['ac_name'] for a in auction.auctioneer.animal_stack.values('ac_name').all()]
        other_players = auction.game_table.players.exclude(player_name=auction.auctioneer.player_name)
        for a_animal in auctioneer_animals:
            for p in other_players:
                if a_animal in [p_animal['ac_name'] for p_animal in p.animal_stack.values('ac_name').all()]:
                    bluff_options.append({p.player_name: a_animal})
        return bluff_options

    def handle(self, *args, **options):
        self.generate_animals()
        # self.generate_money()
