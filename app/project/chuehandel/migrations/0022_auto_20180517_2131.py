# Generated by Django 2.0.5 on 2018-05-17 21:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('chuehandel', '0021_auto_20180517_1705'),
    ]

    operations = [
        migrations.AlterField(
            model_name='auction',
            name='auctioneer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='chuehandel.Player', verbose_name='Auctioneer'),
        ),
    ]
