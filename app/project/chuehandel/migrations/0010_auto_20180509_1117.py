# Generated by Django 2.0.5 on 2018-05-09 11:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chuehandel', '0009_auto_20180509_1115'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gametable',
            name='animal_stack',
            field=models.ManyToManyField(blank=True, to='chuehandel.AnimalCard', verbose_name='Animal stack'),
        ),
        migrations.AlterField(
            model_name='gametable',
            name='money_stack',
            field=models.ManyToManyField(blank=True, to='chuehandel.MoneyCard', verbose_name='Animal stack'),
        ),
    ]
