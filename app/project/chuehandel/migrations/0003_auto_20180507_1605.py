# Generated by Django 2.0.3 on 2018-05-07 16:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chuehandel', '0002_auto_20180507_1604'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='animal_stack',
            field=models.ManyToManyField(to='chuehandel.AnimalCard', verbose_name='Animal stack'),
        ),
        migrations.AlterField(
            model_name='player',
            name='money_stack',
            field=models.ManyToManyField(to='chuehandel.MoneyCard', verbose_name='Animal stack'),
        ),
    ]
