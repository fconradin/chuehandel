from random import randint

from django.db import models
from django.db.models import Count, F, Max

from project.chuehandel.models import AnimalCard, MoneyCard


class Player(models.Model):
    player_name = models.CharField(
        verbose_name='Player name',
        max_length=50,
    )

    animal_stack = models.ManyToManyField(
        to='chuehandel.AnimalCard',
        blank='true',
        verbose_name='Animal stack',
        related_name='players'
    )

    money_stack = models.ManyToManyField(
        to='chuehandel.MoneyCard',
        blank='true',
        verbose_name='Money stack',
        related_name='players'
    )

    created = models.DateTimeField(
        verbose_name='created',
        auto_now_add=True,
    )

    game_table = models.ForeignKey(
        to='chuehandel.GameTable',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name='Game table',
        related_name='players'
    )

    def set_default_money(self):
        players = Player.objects.filter(game_table=self.game_table)
        zeroes = MoneyCard.objects.filter(mc_points=0).exclude(players__in=players)[:2]
        tens = MoneyCard.objects.filter(mc_points=10).exclude(players__in=players)[:4]
        fifties = MoneyCard.objects.filter(mc_points=50).exclude(players__in=players)[:1]
        self.money_stack.add(*zeroes, *tens, *fifties)

    def __str__(self):
        return f'{self.id}: {self.player_name}'

    class Meta:
        unique_together = (("game_table", "player_name"),)


class GameTable(models.Model):
    game_table_name = models.CharField(
        verbose_name='Game table name',
        max_length=50,
        blank=True,
        unique=True
    )

    animal_stack = models.ManyToManyField(
        to='chuehandel.AnimalCard',
        verbose_name='Animal stack',
        blank=True,
    )

    money_stack = models.ManyToManyField(
        to='chuehandel.MoneyCard',
        verbose_name='Money stack',
        blank=True,
    )

    game_running = models.BooleanField(
        verbose_name="Game running",
        default=False
    )

    def set_default_animals(self):
        self.animal_stack.add(*AnimalCard.objects.all()[:4])

    def get_next_auctioneer(self):
        return self.players.all().annotate(nbr=Count('auctions')).order_by('nbr', 'id').first()

    def __str__(self):
        return f'{self.id}: {self.game_table_name}'


class Auction(models.Model):
    ACTION_CHOICES = (
        ("Bluff", "Bluff"),
        ("Auction", "Auction"),
        ("Pass", "Pass"),
    )

    auctioneer = models.ForeignKey(
        to='chuehandel.Player',
        on_delete=models.CASCADE,
        verbose_name='Auctioneer',
        related_name='auctions'
    )

    game_table = models.ForeignKey(
        to='chuehandel.GameTable',
        verbose_name='Game table',
        on_delete=models.CASCADE,
        related_name='auctions'
    )

    action = models.CharField(
        verbose_name='Action',
        choices=ACTION_CHOICES,
        max_length=10,
        blank=True,
        null=True,
    )

    current_animal = models.ManyToManyField(
        verbose_name='Current animal(s)',
        to='chuehandel.Animalcard',
        blank=True,
    )

    created = models.DateTimeField(
        verbose_name='created',
        auto_now_add=True,
    )

    ended = models.BooleanField(
        verbose_name='Auction ended',
        default=False
    )

    bluffed = models.ForeignKey(
        to='chuehandel.Player',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def get_highest_bid(self):
        max_amounts = Bidder.objects.filter(auction=self) \
            .values(player_name=F('player__player_name')) \
            .annotate(max_bid=Max('amount'))
        if max_amounts.count() > 0:
            return {p['player_name']: p['max_bid'] for p in max_amounts}
        else:
            return {}

    def set_current_animal(self):
        animal_stack = self.game_table.animal_stack.all()
        self.current_animal.add(animal_stack[randint(0, len(animal_stack) - 1)])
        self.save()

    def set_action(self, action):
        self.action = action
        self.save()

    def determine_winner(self):
        if self.ended:
            highest_bids = self.get_highest_bid()
            max_bid = max(highest_bids.values())
            for player_name, bid in highest_bids.items():
                if bid == max_bid:
                    return player_name
        else:
            return None

    def get_bluff_options(self):
        bluff_options = []
        auctioneer_animals = self.auctioneer.animal_stack.all()
        other_players = self.game_table.players.exclude(player_name=self.auctioneer.player_name)
        for a_animal in auctioneer_animals:
            for p in other_players:
                if a_animal.ac_name in [p_animal.ac_name for p_animal in p.animal_stack.all()]:
                    bluff_options.append({p.player_name: f'{a_animal.ac_name}({a_animal.ac_points})'})
        return bluff_options

    class Meta:
        ordering = ['created']


class Bidder(models.Model):
    auction = models.ForeignKey(
        to='chuehandel.Auction',
        on_delete=models.CASCADE,
        related_name='bidders'
    )
    player = models.ForeignKey(
        to='chuehandel.Player',
        on_delete=models.CASCADE,
    )
    amount = models.PositiveIntegerField(
        verbose_name='Amount',
    )
    created = models.DateTimeField(
        verbose_name='created',
        auto_now_add=True,
    )
    money_cards = models.ManyToManyField(
        to='chuehandel.MoneyCard',
        blank=True,
        verbose_name='Bluff money cards'
    )

    class Meta:
        ordering = ['created']
