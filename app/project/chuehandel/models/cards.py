# from django.conf import settings
from django.db import models


class AnimalCard(models.Model):
    ac_name = models.CharField(
        verbose_name='Animal card name',
        max_length=50,
    )

    ac_points = models.PositiveIntegerField(
        verbose_name='Animal card points',
    )

    ac_icon = models.CharField(
        verbose_name='Animal card icon',
        max_length=50,
    )

    def __str__(self):
        return f'{self.id}: {self.ac_name} ({self.ac_points})'


class MoneyCard(models.Model):
    mc_name = models.CharField(
        verbose_name='Money card name',
        max_length=50,
    )

    mc_points = models.PositiveIntegerField(
        verbose_name='Money card points',
    )

    def __str__(self):
        return f'{self.id}: {self.mc_name} ({self.mc_points})'
