import jwt
from channels.sessions import CookieMiddleware
from django.conf import settings
from django.db import close_old_connections

from project.chuehandel.models import Player


class AuthMiddleware:
    """
    Custom middleware (insecure) that takes user IDs from the query string.
    """

    def __init__(self, inner):
        # Store the ASGI application we were passed
        self.inner = inner

    def __call__(self, scope):
        # Look up user from query string (you should also do things like
        # check it's a valid user ID, or if scope["user"] is already populated)
        try:
            token = scope['query_string'].decode()
            player_id = jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])['player_id']
            player = Player.objects.get(id=player_id)
            scope['player'] = player
        except jwt.exceptions.DecodeError:
            scope['player'] = None
        except Player.DoesNotExist:
            scope['player'] = None
        except jwt.ExpiredSignatureError:
            scope['player'] = None
        finally:
            close_old_connections()
            return self.inner(scope)


Auth_middleware_stack = lambda inner: CookieMiddleware(AuthMiddleware(inner))  # noqa
