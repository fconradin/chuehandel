from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/table/(?P<game_table_name>[^/]+)/$', consumers.GameConsumer),
]
