from django.contrib import admin

import project.chuehandel.models.cards as cards
import project.chuehandel.models.game as game


class AnimalAdmin(admin.ModelAdmin):
    list_display = ['id', 'ac_name', 'ac_points']


class MoneyAdmin(admin.ModelAdmin):
    list_display = ['id', 'mc_name', 'mc_points']


class PlayerAdmin(admin.ModelAdmin):
    list_display = ['id', 'player_name', 'money_card_stack', 'animal_card_stack', 'game_table', 'created']
    list_filter = ['game_table__game_table_name']

    def money_card_stack(self, player):
        money_stack = player.money_stack.all()
        return " - ".join([f"{c.mc_name}: {c.id}" for c in money_stack])

    def animal_card_stack(self, player):
        animal_stack = player.animal_stack.all()
        return " - ".join([f"{c.ac_name}: {c.id}" for c in animal_stack])


class GameTableAdmin(admin.ModelAdmin):
    list_display = ['id', 'game_table_name', 'players', 'game_running']

    def players(self, game_table):
        players = game_table.players.all()
        return " - ".join([f"{p.player_name}" for p in players])


class AuctionAdmin(admin.ModelAdmin):
    list_display = ['id', 'auctioneer', 'action', 'animals', 'bluffed', 'ended']

    def animals(self, auction):
        animals = auction.current_animal.all().values('ac_name')
        return " - ".join([f"{a['ac_name']}" for a in animals])


class BidderAdmin(admin.ModelAdmin):
    list_display = ['id', 'auction_id', 'player', 'amount', 'created']


admin.site.register(cards.AnimalCard, AnimalAdmin)
admin.site.register(cards.MoneyCard, MoneyAdmin)
admin.site.register(game.Player, PlayerAdmin)
admin.site.register(game.GameTable, GameTableAdmin)
admin.site.register(game.Auction, AuctionAdmin)
admin.site.register(game.Bidder, BidderAdmin)
