# chuehandel/consumers.py

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json

from project.api.game.serializer import GameTableSerializer, PrivatePlayerSerializer
from project.chuehandel.models import GameTable, Auction, Bidder, MoneyCard, Player


class GameConsumer(WebsocketConsumer):
    # def __init__(self):
    #     super().__init__()
    #     self.game_table_name = ""
    #     self.room_group_name = ""

    def connect(self):
        if self.scope['player'] is not None:
            self.game_table_name = self.scope['url_route']['kwargs']['game_table_name']
            self.room_group_name = f'table_${self.game_table_name}'

            # Join room group
            async_to_sync(self.channel_layer.group_add)(
                self.game_table_name,
                self.channel_name
            )
            self.accept()
            self.send_game_info()

        else:
            self.accept()
            self.close(4030)

    def disconnect(self, close_code):
        # Leave room group
        if close_code != 4030:
            async_to_sync(self.channel_layer.group_discard)(
                self.game_table_name,
                self.channel_name
            )

    # Receive message from WebSocket
    def receive(self, text_data=None, bytes_data=None):
        print('receiving', self.scope['player'])
        # place this in the init, but init fails the way I do it. --> ask Colin
        self.players_done = []  # (no cards left in animal stack, no bluff options)
        text_data_json = json.loads(text_data)
        try:
            payload = text_data_json['payload']
        except KeyError:
            pass
            # payload has not been sent from frontend because not necessary
            # print("payload key not sent:", sys.exc_info()[0])

        fn = text_data_json['type'].lower()
        try:
            method = getattr(self, fn)
        except AttributeError:
            raise NotImplementedError(
                f"Class {self.__class__.__name__} does not implement {fn}")
        try:
            method(payload)
        except UnboundLocalError:
            method()

    def send_game_info(self):
        game_table = GameTable.objects.get(game_table_name=self.game_table_name)
        serializer = GameTableSerializer(game_table)
        self.prepare_group_message('_R_GAME_INFO', serializer.data)

    def prepare_group_message(self, frontend_type, payload=None):
        async_to_sync(self.channel_layer.group_send)(
            self.game_table_name,
            {
                'type': 'send_to_instance',
                'frontend_type': frontend_type,
                'payload': payload
            }
        )

    def send_to_instance(self, event):
        frontend_type = event['frontend_type']
        payload = event['payload']

        self.send(text_data=json.dumps({
            'type': frontend_type,
            'payload': payload
        }))

        if frontend_type == '_R_GAME_INFO':
            player = self.scope['player']
            serializer = PrivatePlayerSerializer(player)
            self.send(text_data=json.dumps({
                'type': '_R_PRIVATE_PLAYER_INFO',
                'payload': serializer.data
            }))

    def start_next_round(self):
        game_table = GameTable.objects.get(game_table_name=self.game_table_name)
        next_auctioneer = game_table.get_next_auctioneer()
        auction = Auction.objects.create(auctioneer=next_auctioneer, game_table=game_table)
        bluff_options = auction.get_bluff_options()
        if len(bluff_options) == 0 and game_table.animal_stack.count() > 0:
            auction.set_action('Auction')
            auction.set_current_animal()
        elif len(bluff_options) > 0 and game_table.animal_stack.count() == 0:
            auction.set_action('Bluff')
        elif len(bluff_options) == 0 and game_table.animal_stack.count() == 0:
            self.players_done.append(next_auctioneer)
            if len(self.players_done) < game_table.players.count():
                self.start_next_round()
            else:
                # todo send game winner as payload
                self.prepare_group_message('_R_END_GAME')
                self.disconnect(2000)
                return
        self.prepare_group_message('_R_CLEAR_ERRORS')
        self.send_game_info()

    # all the methods below handle messages received from the frontend

    def _s_start_game(self):
        game_table = GameTable.objects.get(game_table_name=self.game_table_name)
        game_table.game_running = True
        game_table.save()
        auctioneer = game_table.get_next_auctioneer()
        auction = Auction.objects.create(auctioneer=auctioneer, game_table=game_table)
        auction.set_action('Auction')
        auction.set_current_animal()
        self.send_game_info()

    def _s_set_action(self, payload):
        game_table = GameTable.objects.get(game_table_name=self.game_table_name)
        auction = Auction.objects.filter(game_table=game_table, ended=False) \
            .latest('created')
        auction.set_action(payload)
        if payload == 'Auction':
            auction.set_current_animal()
        self.send_game_info()

    def _s_submit_bid(self, payload):
        game_table = GameTable.objects.get(game_table_name=self.game_table_name)
        auction = Auction.objects.filter(game_table=game_table).latest('created')
        amount = payload['amount']
        highest_bids_dict = auction.get_highest_bid()

        if len(highest_bids_dict) > 0:
            # a bid has been placed
            highest_bid = max(highest_bids_dict.values())
            if highest_bid >= amount:
                self.send(text_data=json.dumps({
                    'type': '_R_BIDDING_ERROR',
                }))
                return
        bid = Bidder.objects.create(auction=auction, player=self.scope['player'], amount=payload['amount'])
        bid.save()
        self.send_game_info()

    def _s_end_auction(self):
        game_table = GameTable.objects.get(game_table_name=self.game_table_name)
        auction = Auction.objects.filter(game_table=game_table, ended=False).latest('created')
        auction.ended = True
        auction.save()
        self.send_game_info()

    def _s_complete_auction(self, payload):
        game_table = GameTable.objects.get(game_table_name=self.game_table_name)
        auction = game_table.auctions.latest('created')
        auctioneer = auction.auctioneer
        cards_to_switch_owner = MoneyCard.objects.filter(id__in=[int(k) for k, v in payload['cards'].items() if v])
        auctioneer.money_stack.add(*cards_to_switch_owner)
        auctioneer.save()
        winner = game_table.players.get(player_name=auction.determine_winner())
        winner.money_stack.remove(*cards_to_switch_owner)
        winner.animal_stack.add(*auction.current_animal.all())
        winner.save()
        game_table.animal_stack.remove(*auction.current_animal.all())
        game_table.save()
        self.start_next_round()

    def _s_select_bluffed(self, payload):
        game_table = GameTable.objects.get(game_table_name=self.game_table_name)
        auction = game_table.auctions.latest('created')
        auction.bluffed = Player.objects.get(game_table=game_table, player_name=payload)
        auction.save()
        self.send_game_info()

    def _s_select_animal(self, payload):
        game_table = GameTable.objects.get(game_table_name=self.game_table_name)
        ac_name = payload.split('(')[0]
        auction = game_table.auctions.latest('created')
        auctioneer_animal_stack = auction.auctioneer.animal_stack.filter(ac_name=ac_name)
        bluffed_animal_stack = auction.bluffed.animal_stack.filter(ac_name=ac_name)
        number_of_animals = min(auctioneer_animal_stack.count(), bluffed_animal_stack.count())
        current_animal = list(auctioneer_animal_stack[:number_of_animals]) \
            + list(bluffed_animal_stack[:number_of_animals])
        auction.current_animal.add(*current_animal)
        auction.save()
        self.send_game_info()

    def _s_complete_bluff(self, payload):
        game_table = GameTable.objects.get(game_table_name=self.game_table_name)
        auction = game_table.auctions.latest('created')
        # get ids of money cards, grab instances, determine amount
        money_card_ids = payload['cards'].keys()
        money_cards = MoneyCard.objects.filter(id__in=money_card_ids)
        amount = sum([money_card.mc_points for money_card in money_cards])
        # create bidder instance and add card instances
        bidder = Bidder.objects.create(auction=auction, player=self.scope['player'], amount=amount)
        bidder.money_cards.add(*money_cards)
        bidder.save()
        # determine number of bidders having placed bids
        number_of_bidders = Bidder.objects.filter(auction=auction).values('player').count()
        if number_of_bidders == 2:
            # both players have placed bids, determine winner update animal stacks and start next round
            auction.ended = True
            auction.save()
            # if both place the same amount, winner is determined alphabetically
            winner = Player.objects.get(game_table=game_table, player_name=auction.determine_winner())
            winner.animal_stack.add(*auction.current_animal.all())
            loser_name = auction.bidders.filter(auction=auction).exclude(player=winner) \
                .values('player__player_name').first()['player__player_name']
            loser = Player.objects.get(game_table=game_table, player_name=loser_name)
            loser.animal_stack.remove(*auction.current_animal.all())
            # exchange the money cards
            winner_money_cards = Bidder.objects.get(auction=auction, player=winner).money_cards.all()
            loser_money_cards = Bidder.objects.get(auction=auction, player=winner).money_cards.all()
            winner.money_stack.remove(*winner_money_cards)
            winner.money_stack.add(*loser_money_cards)
            loser.money_stack.remove(*loser_money_cards)
            loser.money_stack.add(*winner_money_cards)
            winner.save()
            loser.save()
            self.start_next_round()

        else:
            # still waiting for other player
            self.send_game_info()
