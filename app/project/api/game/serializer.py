import sys
from datetime import datetime, timedelta

import jwt
from django.conf import settings
from django.db.models import Count
from rest_framework import serializers
from random import randint

from project.chuehandel.models import GameTable, Player, Auction


class GameSerializer(serializers.Serializer):
    host = serializers.BooleanField(
        label='host boolean',
    )
    game_table_name = serializers.CharField(
        label='Game table name',
        required=False,
    )
    player_name = serializers.CharField(
        label='Player name',
        max_length=50
    )

    def validate(self, data):
        if not data['host']:
            try:
                game_table = GameTable.objects.get(game_table_name=data['game_table_name'])
                if len(game_table.players.all()) >= 5:
                    raise serializers.ValidationError({'error': 'There are already 5 players at this table'})
                elif game_table.game_running:
                    raise serializers.ValidationError({'error': 'This game has already started'})
                else:
                    data['game_table'] = game_table
            except GameTable.DoesNotExist:
                raise serializers.ValidationError({'error': 'This is not a valid game table name'})
            try:
                Player.objects.get(player_name=data['player_name'],
                                   game_table__game_table_name=data['game_table_name'])
                raise serializers.ValidationError({'error': 'Player name already taken at this table'})
            except Player.DoesNotExist:
                return data
        else:
            return data

    def create_game(self, validated_data):
        while True:
            game_table_name = randint(1000, 10000)
            try:
                GameTable.objects.get(game_table_name=game_table_name)
            except GameTable.DoesNotExist:
                game_table = GameTable.objects.create(game_table_name=game_table_name)
                break
        game_table .set_default_animals()
        player = Player.objects.create(player_name=validated_data['player_name'], game_table=game_table)
        player.set_default_money()
        expiry = datetime.utcnow() + timedelta(hours=2)
        token = jwt.encode({'player_id': player.id, 'exp': expiry}, settings.SECRET_KEY, algorithm='HS256')
        return {'game_table_name': game_table.game_table_name, 'player': player.player_name, 'token': token}

    def join_game(self, validated_data):
        game_table = GameTable.objects.get(game_table_name=validated_data['game_table_name'])
        player = Player.objects.create(player_name=validated_data['player_name'])
        player.game_table = game_table
        player.set_default_money()
        player.save()
        expiry = datetime.utcnow() + timedelta(hours=2)
        token = jwt.encode({'player_id': player.id, 'exp': expiry}, settings.SECRET_KEY, algorithm='HS256')
        return {'game_table_name': game_table.game_table_name, 'player': player.player_name, 'token': token}


class PublicPlayerSerializer(serializers.ModelSerializer):
    formated_animal_stack = serializers.SerializerMethodField()
    money_stack_size = serializers.SerializerMethodField()

    class Meta:
        model = Player
        fields = ['player_name', 'formated_animal_stack', 'money_stack_size']

    def get_money_stack_size(self, instance):
        return instance.money_stack.all().count()

    def get_formated_animal_stack(self, instance):
        return list(
            instance.animal_stack.all()
            .values('ac_points', 'ac_name', 'ac_icon')
            .annotate(total=Count('ac_points'))
        )


class AnimalRepresentation(serializers.RelatedField):
    def to_representation(self, value):
        return {value.id: {"name": value.ac_name, "points": value.ac_points, "icon": value.ac_icon}}


class MoneyRepresentation(serializers.RelatedField):
    def to_representation(self, value):
        return [value.id, value.mc_points]


class PrivatePlayerSerializer(serializers.ModelSerializer):
    animal_stack = AnimalRepresentation(queryset=Player.animal_stack, many=True)
    money_stack = MoneyRepresentation(queryset=Player.animal_stack, many=True)
    formated_animal_stack = serializers.SerializerMethodField()
    formated_money_stack = serializers.SerializerMethodField()

    class Meta:
        model = Player
        fields = ['player_name', 'animal_stack', 'money_stack', 'formated_animal_stack', 'formated_money_stack']

    def get_formated_money_stack(self, instance):
        money_stack = instance.money_stack.all().values('mc_points').annotate(total=Count('mc_points'))
        return {c['mc_points']: c['total'] for c in money_stack}

    def get_formated_animal_stack(self, instance):
        animal_stack = instance.animal_stack.all().values('ac_points', 'ac_name').annotate(total=Count('ac_points'))
        return {f"{a['ac_name']}({a['ac_points']})": a['total'] for a in animal_stack}


class AuctionSerializer(serializers.ModelSerializer):
    auctioneer = serializers.SerializerMethodField()
    bluffed = serializers.SerializerMethodField()
    bluff_options = serializers.SerializerMethodField()
    # current_animal = AnimalRepresentation(queryset=Auction.current_animal, many=True)
    current_animal = serializers.SerializerMethodField()
    winner = serializers.SerializerMethodField()

    class Meta:
        model = Auction
        fields = ['auctioneer', 'bluffed', 'bluff_options', 'current_animal', 'ended', 'winner']

    def get_auctioneer(self, instance):
        return instance.auctioneer.player_name

    def get_bluffed(self, instance):
        return instance.bluffed.player_name if instance.bluffed else None

    def get_bluff_options(self, instance):
        return instance.get_bluff_options()

    def get_current_animal(self, instance):
        return {a.id: {"name": a.ac_name, "points": a.ac_points, "icon": a.ac_icon}
                for a in instance.current_animal.all()}

    def get_winner(self, instance):
        return instance.determine_winner()


class GameTableSerializer(serializers.ModelSerializer):
    action = serializers.SerializerMethodField()
    players = PublicPlayerSerializer(many=True)
    animal_cards_left = serializers.SerializerMethodField()
    auction = serializers.SerializerMethodField()
    bidders = serializers.SerializerMethodField()

    class Meta:
        model = GameTable
        fields = ['action', 'game_table_name', 'game_running', 'animal_cards_left', 'players', 'auction', 'bidders']

    def get_action(self, instance):
        try:
            action = instance.auctions.latest('created').action
            return action
        except Auction.DoesNotExist:
            return 'Auction'

    def get_animal_cards_left(self, instance):
        return instance.animal_stack.all().count()

    def get_bidders(self, instance):
        try:
            auction = instance.auctions.latest('created')
            return auction.get_highest_bid()
        except Auction.DoesNotExist:
            print("GameTableSerializer.get_bidders:", sys.exc_info()[0])
            return {}

    def get_auction(self, instance):
        try:
            return AuctionSerializer(instance.auctions.latest('created')).data
        except Auction.DoesNotExist:
            return {
                'auctioneer': None,
                'bluff_options': [{}],
                'bluffed': None,
                'current_animal': [{}],
                'ended': False,
                'winner': None,
            }
