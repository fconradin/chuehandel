from rest_framework.generics import GenericAPIView
from rest_framework.response import Response


from project.api.game.serializer import GameSerializer


class Game(GenericAPIView):
    serializer_class = GameSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if serializer.validated_data['host']:
            objects = serializer.create_game(serializer.validated_data)
        else:
            objects = serializer.join_game(serializer.validated_data)
        return Response(objects)
