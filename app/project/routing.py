from channels.routing import ProtocolTypeRouter, URLRouter

from project.chuehandel.middelware import Auth_middleware_stack
from project.chuehandel.routing import websocket_urlpatterns

application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': Auth_middleware_stack(
        URLRouter(
            websocket_urlpatterns
        )
    ),
})
